<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tests', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->references('users')->on('id');
            $table->string('summary', 4)->nullable();
            $table->timestamps();
        });

        Schema::create('user_test_answers', function (Blueprint $table) {
            $table->id();
            $table->integer('test_id')->references('user_tests')->on('id');
            $table->integer('question_id')->references('questions')->on('id');
            $table->tinyInteger('answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_test_answers');
        Schema::dropIfExists('user_tests');
    }
}
