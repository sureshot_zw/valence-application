<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\UserTest;
use App\Models\Questions;

class UserTestAnswersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'test_id' => UserTest::all()->random()->id,
            'question_id' => Questions::all()->random()->id,
            'answer' => rand(1,7)
        ];
    }
}
