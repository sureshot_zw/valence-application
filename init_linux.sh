docker run --rm --interactive --tty --volume $PWD:/app composer create-project

# Start Laravel Sail for the first time
./vendor/bin/sail build --no-cache
./vendor/bin/sail up -d

docker-compose exec laravel.test sh -c "chmod -R 777 /var/www/html/storage/ /var/www/html/storage/*"
docker-compose exec laravel.test sh -c "cd /var/www/html && npm install"
docker-compose exec laravel.test sh -c "cd /var/www/html && npm run dev"

./vendor/bin/sail artisan test
./vendor/bin/sail artisan migrate
./vendor/bin/sail artisan db:seed

