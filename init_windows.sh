#!/bin/bash
docker run --rm --interactive --tty --volume $PWD:/app composer create-project

vendor/bin/sail up -d

vendor/bin/sail npm install 
vendor/bin/sail npm run dev
vendor/bin/sail artisan test
vendor/bin/sail artisan migrate
vendor/bin/sail artisan db:seed