<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\UserTestAnswers;

class UserTest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'summary',
    ];

    public $timestamps = true;

    /**
     * Test belongs to user
     * 
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Test has many answers
     * 
     * @return HasMany
     */
    public function answers()
    {
        return $this->hasMany(UserTestAnswers::class, 'test_id', 'id');
    }
}
