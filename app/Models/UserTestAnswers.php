<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserTest;
use App\Models\Questions;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserTestAnswers extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'test_id',
        'question_id',
        'answer'
    ];

    public $timestamps = true;
    
    /**
     * Answers belong to a test
     * 
     * @return BelongsTo
     */
    public function test()
    {
        return $this->belongsTo(UserTest::class, 'id', 'test_id');
    }

    /**
     * Has one question
     * 
     * @return HasOne
     */
    public function question()
    {
        return $this->hasOne(Questions::class, 'id', 'question_id');
    }
}
