<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    /**
     * Fillable attributes
     * 
     * @var array
     */
    protected $fillable = [
        'question',
        'dimension',
        'direction',
        'meaning',
    ];

    public $timestamps = true;
}
