<?php

namespace App\Http\Controllers\Mbti;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Questions;
use App\Models\User;
use App\Interfaces\UserServiceInterface;
use App\Interfaces\MBTIAnswersInterface;
use App\Http\Requests\SubmitAnswerRequest;

class QuestionsController extends Controller
{
    /**
     * Answer service
     * 
     * @var MBTIAnswersInterface
     */
    protected $answers;

    /**
     * User service 
     * 
     * @var UserServiceInterface
     */
    protected $users;

    /**
     * Default contructor for handling content requests for questions and 
     * processing answer responses
     * 
     * @param UserServiceInterface $users
     * @param MBTIAnswersInterface $answers
     */
    public function __construct(UserServiceInterface $users, MBTIAnswersInterface $answers){
        $this->users = $users;
        $this->answers = $answers;
    }
    /**
     * Return page for questions
     * 
     * @return View
     */
    public function index()
    {
        return view('mbti.questions');
    }

    /**
     * Get list of question for FE display
     * 
     * @return Response
     */
    public function questions()
    {
        return response()->json([
            'questions' => Questions::all()->keyBy('id')->sortBy('id')
        ], Response::HTTP_OK);
    }

    /**
     * Process answers submitted by a user
     * 
     * @param SubmitAnswerRequest $request
     * 
     * @return Response
     */
    public function answers(SubmitAnswerRequest $request)
    {
        $data = $request->validated();
        $user = $this->users->create($data['email_address']);

        if($this->answers->add($user, $data['answers'])) {
            return response([
                'link' => route('results', compact('user')),
            ]);
        }
    }
    
    /**
     * Return page for results
     * 
     * @return View
     */
    public function results(User $user)
    {
        return view('mbti.results', compact('user')); 
    }

    /**
     * Return page for results
     * 
     * @return Response
     */
    public function summary(User $user)
    {
        return response($this->answers->summary($user));
    }
}
