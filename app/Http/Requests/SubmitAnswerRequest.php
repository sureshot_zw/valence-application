<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmitAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'answers' => array_filter($this->answers, function($question) {
                return !empty($question);
            }),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email_address' => 'required|email',
            'answers' => 'required|array|size:10',
            'answers.*' => 'required|numeric',
        ];
    }

    /**
     * Attribute names for invalid request responses
     * 
     * @return array
     */
    public function attributes()
    {
        return [
            'email_address' => 'Email Address',
            'answers' => 'Answers',
        ];
    }
}
