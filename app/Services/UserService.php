<?php
namespace App\Services;

use App\Interfaces\UserServiceInterface;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash
;
class UserService implements UserServiceInterface {
    /**
     * Create/Get an instance of a user based on email address
     * 
     * @param string $email
     * 
     * @return $user
     */
    public function create(string $email)
    {
        // we are going to use the user relation to loosely keep track of tests a user taks
        // we will asign a random name as its just a placeholder and we will use the 
        // email as source of truth in terms of user -> test associations
        return User::firstOrCreate([
            'email' => $email,
        ],[
            'name' => Str::random(),
            'password' => Hash::make(Str::random(8)),
        ]);
    }
}