<?php
namespace App\Services;

use App\Interfaces\MBTIAnswersInterface;
use App\Models\User;
use App\Models\Questions;

class MBTIAnswersService implements MBTIAnswersInterface {
    /**
     * Create/Get an instance of a user based on email address
     * 
     * @params User $user
     * @params array $answers
     * 
     * @return bool
     */
    public function add(User $user, array $answers) : bool
    {
        $userTest = $user->tests()->create([
            'user_id' => $user->id,
        ]);

        foreach($answers as $question => $answer) {
            $userTest->answers()->create([
                'test_id' => $userTest->id,
                'question_id' => $question,
                'answer' => $answer,
            ]);
        }

       return true;
    }

    /**
     * Determine summary for users latest test
     * 
     * @return array
     */
    public function summary(User $user) : array
    {
        $allDimensions = []; 
        $summaryDimension = [];

        $user->latestTest
        ->answers
        ->each(function($answer) use(&$summaryDimension, &$allDimensions) {
            $question = $answer->question()->first();
            $allDimensions[] = $question->dimension;
            $dimension = str_split($question->dimension, 1);
            $direction = $question->direction;
            $determinedDimension = $dimension[0];

            // if the answer is neutral we assume left value of the dimension
            if($answer->answer === 4) return; 
            
            // if the answer is greater than 4 and is a positive direction assume right value of dimension
            if($answer->answer > 4) $determinedDimension = $dimension[1];

            // based on dimension and answer take the opposite
            if($direction < 0 && $answer->answer > 4) $determinedDimension = $dimension[0];
            if($direction < 0 && $answer->answer <= 4)$determinedDimension = $dimension[1];

            $summaryDimension[] = $determinedDimension;
        });

        return $this->determineMBTISummary($summaryDimension, array_unique($allDimensions));
    }

    /**
     * Determine MBTI summary for user test
     * 
     * @param array $summaryDimension
     * @param array $allDimensions
     * 
     * @return array
     */
    protected function determineMBTISummary(array $summaryDimension, array $allDimensions) : array
    {
        $firstDimensionCount = 0;
        $secondDimensionCount = 0;
        $mbtiSummary = [];

        foreach($allDimensions as $dimension) {
            $splitDimension = str_split($dimension, 1);
            foreach($summaryDimension as $summary) {
                // we determine which dimension the user leaning more towards
                if($summary == $splitDimension[0]) {
                    $firstDimensionCount ++;
                }

                if($summary == $splitDimension[1]) {
                    $secondDimensionCount ++;
                }
            }

            if($firstDimensionCount > $secondDimensionCount || 
            $firstDimensionCount == $secondDimensionCount) {
                $mbtiSummary[] = $splitDimension[0];
            }

            if($firstDimensionCount < $secondDimensionCount) {
                $mbtiSummary[] = $splitDimension[1];
            }

            $firstDimensionCount = 0;
            $secondDimensionCount = 0;
        }

        return [
            'summary_components' => $mbtiSummary,
            'summary_title' => implode("", $mbtiSummary)
        ];
    }
}