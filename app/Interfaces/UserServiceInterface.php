<?php
namespace App\Interfaces;

interface UserServiceInterface {
    public function create(string $email);
}