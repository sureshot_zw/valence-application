<?php
namespace App\Interfaces;

use App\Models\User;

interface MBTIAnswersInterface {
    public function add(User $user, array $answers) : bool;
    public function summary(User $user) : array;
}