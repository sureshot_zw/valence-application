<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserAnswerAndResultsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Setup override
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed');
    }
    
    /**
     * @covers QuestionsController::answers
     */
    public function test_user_can_submit_answers()
    {
        $user = User::factory()->make()->toArray();

        $this->put('/questions/answers', [
            "email_address" => $user['email'],
            "answers" => [
                0 => null,
                1 => "1",
                2 => "1",
                3 => "1",
                4 => "1",
                5 => "1",
                6 => "1",
                7 => "1",
                8 => "1",
                9 => "1",
                10 => "1"
            ],
        ])->assertSuccessful();
    }

    /**
     * @covers QuestionsController::answers
     */
    public function test_user_cannot_submit_answers_without_an_email_address()
    {
        $this->put('/questions/answers', [
            "answers" => [
                1 => "1",
            ],
        ])->assertStatus(302);
    }

        /**
     * @covers QuestionsController::summary
     */
    public function test_user_can_get_mbti_result_summary()
    {
        $user = User::factory()->make()->toArray();

        $this->put('/questions/answers', [
            "email_address" => $user['email'],
            "answers" => [
                0 => null,
                1 => "7",
                2 => "7",
                3 => "7",
                4 => "7",
                5 => "7",
                6 => "7",
                7 => "7",
                8 => "7",
                9 => "7",
                10 => "7"
            ],
        ]);
        
        $createdUser = User::where('email', $user['email'])->first();
        $response = $this->get('/results/' . $createdUser->id . '/mbti')->json();

        $this->assertArrayHasKey('summary_components', $response);
        $this->assertArrayHasKey('summary_title', $response);
    }
}
