<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\UserTestAnswers;
use App\Models\Questions;
use App\Interfaces\MBTIAnswersInterface;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MBTISummaryTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * Setup override
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed');
    }
    
    /**
     * @covers MBTIAnswersService::summary
     */
    public function test_mbit_can_generate_summary_for_entp_test()
    {
        $answers = [
            1 => 4,
            2 => 3,
            3 => 1,
            4 => 6,
            5 => 7,
            6 => 3,
            7 => 5,
            8 => 3,
            9 => 6,
            10 => 6
        ];
        $user = User::factory()->create();
        $userTest = $user->tests()->create([
            'user_id' => $user->id,
        ]);

        Questions::all()
        ->sortBy('id')
        ->each(function($question, $index) use ($userTest, $answers) {
            UserTestAnswers::factory()->create([
                'test_id' => $userTest->id,
                'question_id' => $question->id,
                'answer' => $answers[$question->id]
            ]);
        });

        $this->assertEquals('ENTP', app()->make(MBTIAnswersInterface::class)->summary($user)['summary_title']);
    }

    /**
     * @covers MBTIAnswersService::summary
     */
    public function test_mbit_can_generate_summary_for_estj_test()
    {
        $answers =[
            1 => 1,
            2 => 5,
            3 => 4,
            4 => 6,
            5 => 5,
            6 => 2,
            7 => 6,
            8 => 3,
            9 => 3,
            10 => 2
        ];
        $user = User::factory()->create();
        $userTest = $user->tests()->create([
            'user_id' => $user->id,
        ]);

        Questions::all()
        ->sortBy('id')
        ->each(function($question, $index) use ($userTest, $answers) {
            UserTestAnswers::factory()->create([
                'test_id' => $userTest->id,
                'question_id' => $question->id,
                'answer' => $answers[$question->id]
            ]);
        });

        $this->assertEquals('ESTJ', app()->make(MBTIAnswersInterface::class)->summary($user)['summary_title']);
    }

    /**
     * @covers MBTIAnswersService::summary
     */
    public function test_mbit_can_generate_summary_for_infp_test()
    {
        $answers =[
            1 => 3,
            2 => 2,
            3 => 6,
            4 => 1,
            5 => 7,
            6 => 3,
            7 => 2,
            8 => 5,
            9 => 2,
            10 => 7
        ];
        $user = User::factory()->create();
        $userTest = $user->tests()->create([
            'user_id' => $user->id,
        ]);

        Questions::all()
        ->sortBy('id')
        ->each(function($question, $index) use ($userTest, $answers) {
            UserTestAnswers::factory()->create([
                'test_id' => $userTest->id,
                'question_id' => $question->id,
                'answer' => $answers[$question->id]
            ]);
        });

        $this->assertEquals('INFJ', app()->make(MBTIAnswersInterface::class)->summary($user)['summary_title']);
    }

    /**
     * @covers MBTIAnswersService::summary
     */
    public function test_mbit_can_generate_summary_for_estj_test_with_neutral_selection()
    {
        $answers =[
            1 => 4,
            2 => 4,
            3 => 4,
            4 => 4,
            5 => 4,
            6 => 4,
            7 => 4,
            8 => 4,
            9 => 4,
            10 => 4
        ];
        $user = User::factory()->create();
        $userTest = $user->tests()->create([
            'user_id' => $user->id,
        ]);

        Questions::all()
        ->sortBy('id')
        ->each(function($question, $index) use ($userTest, $answers) {
            UserTestAnswers::factory()->create([
                'test_id' => $userTest->id,
                'question_id' => $question->id,
                'answer' => $answers[$question->id]
            ]);
        });

        $this->assertEquals('ESTJ', app()->make(MBTIAnswersInterface::class)->summary($user)['summary_title']);
    }

    /**
     * @covers MBTIAnswersService::summary
     */
    public function test_mbit_can_generate_summary_for_istj_test_with_not_agreeable_selection()
    {
        $answers =[
            1 => 1,
            2 => 1,
            3 => 1,
            4 => 1,
            5 => 1,
            6 => 1,
            7 => 1,
            8 => 1,
            9 => 1,
            10 => 1
        ];
        $user = User::factory()->create();
        $userTest = $user->tests()->create([
            'user_id' => $user->id,
        ]);

        Questions::all()
        ->sortBy('id')
        ->each(function($question, $index) use ($userTest, $answers) {
            UserTestAnswers::factory()->create([
                'test_id' => $userTest->id,
                'question_id' => $question->id,
                'answer' => $answers[$question->id]
            ]);
        });

        $this->assertEquals('ISTJ', app()->make(MBTIAnswersInterface::class)->summary($user)['summary_title']);
    }

    /**
     * @covers MBTIAnswersService::summary
     */
    public function test_mbit_can_generate_summary_for_estp_test_with_agreeable_selection()
    {
        $answers =[
            1 => 7,
            2 => 7,
            3 => 7,
            4 => 7,
            5 => 7,
            6 => 7,
            7 => 7,
            8 => 7,
            9 => 7,
            10 => 7
        ];
        $user = User::factory()->create();
        $userTest = $user->tests()->create([
            'user_id' => $user->id,
        ]);

        Questions::all()
        ->sortBy('id')
        ->each(function($question, $index) use ($userTest, $answers) {
            UserTestAnswers::factory()->create([
                'test_id' => $userTest->id,
                'question_id' => $question->id,
                'answer' => $answers[$question->id]
            ]);
        });

        $this->assertEquals('ESTP', app()->make(MBTIAnswersInterface::class)->summary($user)['summary_title']);
    }
}
