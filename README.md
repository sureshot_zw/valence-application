# Valence MBTI Application

This application is a personality questionnaire application that allows a user to answer a few questions which
are then used t0 determine the user’s personality type. 

## Working assumptions made
1. A working assumption made is that for displaying results we are only concerned with the last test taken by the user. Though each test taken by a user is persisted to the database, for displaying results we only show the MBTI summary for the last test taken. With more time I would look to allow specific test results to be requested and viewed. 
1. The expected summary for unit test detailed `test_mbit_can_generate_summary_for_infp_test` is `INFP` though the currenty the algorithm and hand calculations evaluate the answer to be `INFJ`. It's possible I am not quite correct on a finner grain of the algorithm. All other unit tests evaluate the expected summaries correctly.  
1. As the application is not using any User based authentication, requests between the VueJs and laravel use the `csrf-token` as a form of authentication to guard against `CORS` requests. With more time I would look to integrate the use of Passport or Sanctum for bearer token authentication for an added layer of security for requests. 

## How to get the application running
1. Clone the repository `git clone https://gitlab.com/sureshot_zw/valence-application.git`.
1. Depending on the operating system you are using, do one of the following:
    1. If you are using a Linux based system, navigate to the project directory i.e. `cd valence-application` and run `sudo sh init_linux.sh`.
    1. If you are using a Windows based system with the Windows Subsystem for Linux, navigate to the project directory i.e. `cd valence-application` within the linux subsystem (type `wsl` within your power shell to gain access to the linux containers) and run `sudo sh init_windows.sh`. 
    1. Running as sudo helps get around some permissions issues that occur while building docker containers as well as setting permissions to certain folders.
1. This will run a shell script that will setup the application, install composer dependencies, run migrations and seeders as well as run unit tests using an in-memory database. Take note initial setup may take a bit of time but subsequent runs will be fast. 
1. Once done you should be able to visit the application in your browser on http://localhost
1. After the intial run, you can bring down the application by running `vendor/bin/sail down` within the project folder. 
1. To start up the project again you just run `vendor/bin/sail up -d` within the project folder.
1. To run unit tests just run `vendor/bin/sail artisan test` within the project folder (though they are run as part of building the project by the initialization scripts). 

## Trouble shooting

1. In the off chance you fail to run the initialisation scripts you can just copy the following and run it directly in your terminal. For windows:
    ```
    docker run --rm --interactive --tty --volume $PWD:/app composer create-project && \
    vendor/bin/sail up -d && \
    vendor/bin/sail npm install && \
    vendor/bin/sail npm run dev && \
    vendor/bin/sail artisan test && \
    vendor/bin/sail artisan migrate && \
    vendor/bin/sail artisan db:seed
    ```
1. For linux:
    ```
    docker run --rm --interactive --tty --volume $PWD:/app composer create-project && \
    ./vendor/bin/sail build --no-cache && \
    ./vendor/bin/sail up -d && \
    docker-compose exec laravel.test sh -c "chmod -R 777 /var/www/html/storage/ /var/www/html/storage/*" && \
    docker-compose exec laravel.test sh -c "cd /var/www/html && npm install" && \
    docker-compose exec laravel.test sh -c "cd /var/www/html && npm run dev" && \
    ./vendor/bin/sail artisan test && \
    ./vendor/bin/sail artisan migrate && \
    ./vendor/bin/sail artisan db:seed
    ```


