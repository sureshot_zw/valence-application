<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\Mbti\QuestionsController::class, 'index'])->name('questions');

Route::prefix('results')->group(function () {
    Route::get('/{user}', [App\Http\Controllers\Mbti\QuestionsController::class, 'results'])->name('results');
    Route::get('/{user}/mbti', [App\Http\Controllers\Mbti\QuestionsController::class, 'summary'])->name('summary');
});

Route::prefix('questions')->group(function () {
    Route::get('/', [App\Http\Controllers\Mbti\QuestionsController::class, 'questions'])->name('questionsList');
    Route::put('/answers', [App\Http\Controllers\Mbti\QuestionsController::class, 'answers'])->name('answers');
});